# ssh_tab.py

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from common import systemd, linux_modules
import subprocess

class SSHTab(Gtk.VBox):
    def __init__(self):
        super().__init__()
        self.set_border_width(10)

        # Create an HBox for the SSH switch
        ssh_switch_box = Gtk.HBox()
        ssh_switch_label = Gtk.Label(label="Enable/Disable SSH")
        self.ssh_switch = Gtk.Switch()  # Switch for Enable/Disable SSH
        self.ssh_switch.set_active(systemd.is_service_enabled("ssh"))

        ssh_switch_box.pack_start(ssh_switch_label, False, False, 0)
        ssh_switch_box.pack_start(self.ssh_switch, False, False, 0)
        ssh_switch_box.set_border_width(20)

        # Increase spacing between the label and switch
        ssh_switch_label.set_margin_right(20)  # Increase the right margin
        self.ssh_switch.connect("notify::active",
                                lambda switch, gparam: systemd.toggle_service(switch, gparam, "ssh"))

        self.pack_start(ssh_switch_box, False, False, 0)
