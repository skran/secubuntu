# secubuntu

A GTK3 GUI program to harden your Ubuntu installation.<br>
**This project is still a Work in Progress!**

## Features
### USB denylisting
- Sets up your system to only connect to the USB devices you need.<br><br>
<img src='./images/usb_tab.webp' width="51%" height="auto">


### Disabling Camera
- Allows you to disable/enable the camera module.

### GUI for Firewall and SSH configuration
- Convenient if you don't like the terminal.
