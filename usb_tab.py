import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib
import subprocess
from common import systemd, usb

class UsbTab(Gtk.VBox):

    def __init__(self):
        super().__init__()
        self.set_spacing(12)

        # Create an HBox for the USB switch
        header_box = Gtk.HBox()
        usb_switch_label = Gtk.Label(label="Enable USBGuard")
        self.usb_switch = Gtk.Switch()
        self.usb_switch.set_active(systemd.is_service_enabled("usbguard"))

        header_box.pack_start(usb_switch_label, False, False, 0)
        header_box.pack_start(self.usb_switch, False, False, 0)
        header_box.set_border_width(20)
        usb_switch_label.set_margin_right(20)  # Increase the right margin
        self.usb_switch.connect("notify::active",
                                lambda switch, gparam: systemd.toggle_service(switch, gparam, "usbguard"))

        self.pack_start(header_box, False, False, 0)
        header_box = Gtk.HBox()
        label = Gtk.Label()
        label.set_markup("<b>Allow USB Devices</b>")
        self.pack_start(label, False, False, 0)

        label = Gtk.Label()
        label.set_text("Only the below listed USB devices will be allowed to interact with the system.\n"+
                       "Connect all the USB devices you need allowance for and click on the button.\n"+
                       "You can run this program each time you need to allow a new device.\n")
        label.set_xalign(0)
        self.pack_start(label, False, False, 0)

        self.device_liststore = Gtk.ListStore(str)
        device_listview = Gtk.TreeView(model=self.device_liststore)

        renderer_text = Gtk.CellRendererText()
        column_text = Gtk.TreeViewColumn("Device Name", renderer_text, text=0)
        device_listview.append_column(column_text)

        self.pack_start(device_listview, True, True, 0)

        generate_button = Gtk.Button(label="Allow these devices")
        generate_button.connect("clicked", self.on_generate_clicked)
        self.pack_start(generate_button, False, False, 0)

        def repeat():
            return usb.list_usb_devices(self.device_liststore, self.usb_switch, False)
        GLib.timeout_add_seconds(1, repeat)

    def on_generate_clicked(self, widget):
        command = "usbguard generate-policy > /etc/usbguard/rules.conf && systemctl restart usbguard"
        result = subprocess.run(["pkexec", "bash", "-c", command])
        message = "USBGuard ruleset generated and saved to /etc/usbguard/rules.conf"
        print("Result of pkexec: ", result)
        if result.returncode != 0:
            message = "Error, please try again"
        dialog = Gtk.MessageDialog(self.get_toplevel(),
                                   0,
                                   Gtk.MessageType.INFO,
                                   Gtk.ButtonsType.OK,
                                   message)
        dialog.run()
        dialog.close()

