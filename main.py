import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, Pango
import subprocess
from ssh_tab import SSHTab
from usb_tab import UsbTab
from cam_tab import CamTab

class Main(Gtk.Window):
    def __init__(self):
        super().__init__(title="Configuration GUI")
        self.set_border_width(10)
        notebook = Gtk.Notebook()
        notebook.set_tab_pos(Gtk.PositionType.LEFT)
        self.add(notebook)

        # Network tab
        config_tab = Gtk.VBox()
        config_tab.set_border_width(10)

        # Create the SSH tab's content using the imported function
        ssh_tab = SSHTab()

        # Create an HBox for the "Set Firewall Rules" button
        firewall_box = Gtk.HBox()
        firewall_btn = Gtk.Button.new_with_label("Set Firewall Rules")
        firewall_btn.set_hexpand(False)  # Disable horizontal expansion

        # Connect the "clicked" signal to a function that runs gufw
        firewall_btn.connect("clicked", self.run_gufw)

        firewall_box.pack_start(firewall_btn, False, False, 0)
        firewall_box.set_border_width(20)
        config_tab.pack_start(ssh_tab, False, False, 0)
        config_tab.pack_start(firewall_box, False, False, 0)

        notebook.append_page(config_tab, Gtk.Label(label="<b>Network</b>"))
        notebook.get_tab_label(config_tab).set_use_markup(True)

        # USB tab
        usb_tab = UsbTab()
        usb_tab.set_border_width(10)
        # Create a horizontal box for the USB switch
        notebook.append_page(usb_tab, Gtk.Label(label="<b>USB</b>"))
        notebook.get_tab_label(usb_tab).set_use_markup(True)

        # Cam tab
        cam_tab = CamTab()
        cam_tab.set_border_width(10)
        notebook.append_page(cam_tab, Gtk.Label(label="<b>Camera</b>"))
        notebook.get_tab_label(cam_tab).set_use_markup(True)

        self.show_all()

    def run_gufw(self, widget):
        try:
            subprocess.Popen(["gufw"])
        except FileNotFoundError:
            print("gufw is not installed.")


if __name__ == "__main__":

    settings = Gtk.Settings.get_default()
    font_desc = Pango.FontDescription()
    font_desc.set_size(15 * Pango.SCALE)

    settings.set_property("gtk-font-name", font_desc.to_string())

    gui = Main()
    gui.connect("destroy", Gtk.main_quit)
    Gtk.main()

