import subprocess

def is_service_enabled(unit_name) -> bool:
    result = subprocess.run(['systemctl', 'is-active', unit_name])
    return result.returncode == 0

def toggle_service(switch, gparam, unit_name):
    state = switch.get_active()
    action = "start" if state else "stop"

    result = subprocess.Popen(["pkexec", "systemctl", action, unit_name])
    result_code = result.wait()
    if result_code == 0:
        print(unit_name + f" toggled to {state}.")
    else:
        print(f"Error while toggling {unit_name}")
