import subprocess

def list_usb_devices(device_liststore, usb_switch, camera_flag) -> bool:
    devices = subprocess.check_output(["lsusb"]).decode("utf-8").split('\n')
    if camera_flag:
        devices = filter(lambda x: True if "amera" in x else False, devices)
    device_liststore.clear()
    if usb_switch.get_active():
        for device in devices:
            device_liststore.append([device])
    print(device_liststore)
    return True
