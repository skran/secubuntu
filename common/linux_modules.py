import subprocess

def is_module_active(module_name) -> bool:
    result = subprocess.check_output(['lsmod']).decode('utf-8').split('\n')
    for line in result:
        if 'uvcvideo' in line:
            return True
    return False

def toggle_module(switch, module_name):
    state = switch.get_active()
    if state:
        result = subprocess.Popen(["pkexec", "modprobe", module_name])
    else:
        result = subprocess.Popen(["pkexec", "rmmod", "--force", module_name])
    result_code = result.wait()
    if result_code == 0:
        print(module_name + f" toggled to {state}.")
    else:
        print(f"Error while toggling {module_name}")

