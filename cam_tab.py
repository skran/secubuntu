import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib
import subprocess
from common import systemd, usb, linux_modules

class CamTab(Gtk.VBox):

    def __init__(self):
        super().__init__()
        self.set_spacing(12)

        # Create an HBox for the USB switch
        header_box = Gtk.HBox()
        usb_switch_label = Gtk.Label(label="Enable Cameras")
        self.usb_switch = Gtk.Switch()
        self.usb_switch.set_active(linux_modules.is_module_active("uvcvideo"))

        header_box.pack_start(usb_switch_label, False, False, 0)
        header_box.pack_start(self.usb_switch, False, False, 0)
        header_box.set_border_width(20)
        usb_switch_label.set_margin_right(20)  # Increase the right margin
        self.usb_switch.connect("notify::active",
                                lambda switch, gparam: linux_modules.toggle_module(switch, "uvcvideo"))

        self.pack_start(header_box, False, False, 0)
        header_box = Gtk.HBox()
        label = Gtk.Label()
        label.set_markup("<b>Allow Cameras</b>")
        self.pack_start(label, False, False, 0)

        self.device_liststore = Gtk.ListStore(str)
        device_listview = Gtk.TreeView(model=self.device_liststore)

        renderer_text = Gtk.CellRendererText()
        column_text = Gtk.TreeViewColumn("Device Name", renderer_text, text=0)
        device_listview.append_column(column_text)

        self.pack_start(device_listview, True, True, 0)

        def repeat():
            return usb.list_usb_devices(self.device_liststore, self.usb_switch, True)
        generate_button = Gtk.Button(label="Allow these cameras")
        generate_button.connect("clicked", self.allow_cameras)
        self.pack_start(generate_button, False, False, 0)

        GLib.timeout_add_seconds(1, repeat)

    def allow_cameras(self, widget):
        list_store_data = [list(row) for row in self.device_liststore]
        print(list_store_data)
        ids = list(map(lambda x: x.split()[6], list_store_data))

        command = "usbguard allow-device "
        for i in ids:
            result = subprocess.run(["pkexec", "bash", "-c", command + i])
        message = "Operation Successful"
        print("Result of pkexec: ", result)
        if result.returncode != 0:
            message = "Error, please try again"
        dialog = Gtk.MessageDialog(self.get_toplevel(),
                                   0,
                                   Gtk.MessageType.INFO,
                                   Gtk.ButtonsType.OK,
                                   message)
        dialog.run()
        dialog.close()
